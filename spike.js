//TODO: some fields have e_ or c_ or arentproperlycamelcased to being with
//TODO: Add config for database connection
import pgStructure from "pg-structure";
const camelCase = require('camelcase');
import { pascalCase } from "pascal-case";
const fs = require('fs');




let createType = (columnName, columnType) => {
  let columnNameCC = camelCase(
    columnName.replace("e_", "").replace("c_",  "")
  );

  const suryaType = getSuryaType(columnType)

  return `${columnNameCC}:{
    type: ${suryaType}, 
    asName: "${columnName}"
  }`;
}


let generateColumnNames = (columns) =>{
  
  const columnNames = columns.map(c => {
    // console.log(c);
    return createType(c.name, c.type.shortName);
  });

  return columnNames;
}

const formatModelName = (modelName) => {
  if(modelName[modelName.length-1] === 's'){
    modelName = modelName.substring(0, modelName.length - 1);
  }

  return pascalCase(modelName);
}

const generateTypeSpec = (table) => {
  let typeClass = ``
  let typeSpec = `
  import { BaseType, Permissions } from "@brownpapertickets/surya-gql-types"
  import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

  export class ${formatModelName(table.name)} extends BaseType {
    constructor(data = {}) {
      super(data)
    }

    childTypes() {
      return { }
    }

  
  typeSpec() {
    return {
      name: "${table.name}",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        secure: ["ADMINISTRATOR"],
      },
      fields: {
        ${generateColumnNames(table.columns)}
      }
    }
  }
}`;

    return typeSpec
}


const generateAPI = (tableName) => {

  let modelName = formatModelName(tableName);

  return `import { BasePGSqlAPI } from "@brownpapertickets/surya-gql-data-pgsql"

  export class ${modelName}API extends BasePGSqlAPI {
    constructor(container, type, typeMap, config) {
      super(container, type, typeMap, config)
      this.log.info('${modelName}API create')
    }
  }
  `;
}

//todo possibly camelCase
const formatFolderName = (modelName) =>{
  if(modelName[modelName.length-1] === 's'){
    modelName = modelName.substring(0, modelName.length - 1);
  }
  return camelCase(modelName);
}

//more agnostic
const generateManifest = (tableName)=> {
  const modelName = formatModelName(tableName);
  return `export const Manifest = {
    sysVersion: "1.0.0",
    typeVersion: "1.0.0",
    description: "${modelName}s objects based on ${tableName} table",
    types: ["${modelName}"],
    queries: "${modelName}Queries",
    resolvers: "${modelName}Resolvers",
    manager: "${modelName}Manager",
    data: {
      interface: "PGSql",
      api: "${modelName}API",
      config: { table: "${tableName}", getTotal: true },
    }
  }`
}

const generateResolver = (tableName) => {
  const modelName = formatModelName(tableName);
  return `export const ${modelName}Resolvers = {
    Query: {
      ${modelName}s: async (_, args, context) => {
        return await context.managers
          .get("${modelName}")
          .query(args, context, { doAuth: true })
      },
      ${modelName}: async (_, args, context) => {
        return await context.managers
          .get("${modelName}")
          .get(args, context, { doAuth: true })
      },
    }
  }`
}

const generateIndex = (tableName) =>{
  let modelName = formatModelName(tableName);
  let folderName = formatFolderName(tableName);

  return `export { Manifest } from "./manifest"
  export { ${modelName} } from "./${folderName}"
  export { ${modelName}Manager } from "./${folderName}Manager"
  export { ${modelName}Queries } from "./${folderName}Queries"
  export { ${modelName}Resolvers } from "./${folderName}Resolvers"
  export { ${modelName}API } from "./${folderName}API"
  `;
}

const generateQueries = (tableName) => {

  let modelCamel = formatFolderName(tableName);
  let modelPascal = formatModelName(tableName);
  
  return `
  import { gql } from "apollo-server"

  export const ${modelPascal}Queries = gql\`
  extend type Query {
    ${modelCamel}s(
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): ${modelPascal}SetResponse
    ${modelCamel}(id: ID!): ${modelPascal}
  }
  \`
`
}

async function demo(tableName) {
  //todo add in config such as node config
  const config = {
    host: '',
    port: 0,
    user: '',
    password: '',
    database: ""
  }
  const db = await pgStructure(config,{ name: "BPT", includeSchemas: ["public"] });

  const table = db.tables.get(tableName);

  let typeSpec = generateTypeSpec(table);

  let folderName = formatFolderName(tableName);

  //create folder
  fs.mkdir(`./${folderName}`, { recursive: true }, (err) => {
    if (err) throw err;
  });

  //create model type
  fs.writeFile(`./${folderName}/${formatModelName(tableName)}.js`, typeSpec, (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;
  });

  //create manifest
  fs.writeFile(`./${folderName}/manifest.js`, generateManifest(tableName), (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;
  });

  //generate index
  fs.writeFile(`./${folderName}/index.js`, generateIndex(tableName), (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;
  });

  //generate api
  fs.writeFile(`./${folderName}/${formatModelName(tableName)}API.js`, generateAPI(tableName), (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;
  });

  //generate resolver

  fs.writeFile(`./${folderName}/${formatModelName(tableName)}Resolvers.js`, generateResolver(tableName), (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;
  });

  //generate queries
  fs.writeFile(`./${folderName}/${formatModelName(tableName)}Queries.js`, generateQueries(tableName), (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;
  });
}

const getSuryaType = (dbType) =>{
  let suryaType  = '';
  switch(dbType){
    case "char":
    case "varchar":
    case "text":
      suryaType = "ScalarTypes.String";
      break;
    case "":
      suryaType = "ScalarTypes.Float";
      break;
    case "timestamp":
    case "date":
    case "time":
      suryaType = "ScalarTypes.DateTime";
      break;
    case "bool":
      suryaType = "ScalarTypes.Boolean";
      break;
    default:
      suryaType = "undefined";
      break;
  }

  return suryaType;
}


demo("events");